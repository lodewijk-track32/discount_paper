#!/usr/bin/env python

'''
The code was successfully tested in PyTorch v1.2.0 with torchvision v0.4.0
Pandas version 0.24.2
OpenCV version 4.1.1



Built-in warnings:
- No input folder found
- Input folder empty

This code is based on the work of Ayoosh Kathuria (see https://github.com/ayooshkathuria/pytorch-yolo-v3)
'''
import time
import torch
import torch.nn as nn
from torch.autograd import Variable
import numpy as np
import cv2
from util import *
import argparse
import os
import os.path as osp
from darknet import Darknet
from preprocess import prep_image, inp_to_image
import pandas as pd
import random
import pickle as pkl
import itertools
import collections
import csv
import warnings
import sys
from pathlib import PurePath

allow_CUDA = True # Enable/disable use of CUDA. System requires roughtly 2GB of GPU-memory. Disable if a 'CUDA out of memory-error' is displayed

warnings.filterwarnings("ignore")

torch.set_num_threads(4)

class test_net(nn.Module):
    def __init__(self, num_layers, input_size):
        super(test_net, self).__init__()
        self.num_layers= num_layers
        self.linear_1 = nn.Linear(input_size, 5)
        self.middle = nn.ModuleList([nn.Linear(5,5) for x in range(num_layers)])
        self.output = nn.Linear(5,2)

    def forward(self, x):
        x = x.view(-1)
        fwd = nn.Sequential(self.linear_1, *self.middle, self.output)
        return fwd(x)

if __name__ ==  '__main__':

    scales = "1,2,3"

    images = "../input" # input folder for images
    batch_size = int(1) # one image per detection round
    confidence = float(0.06) # the threshold P-value in the paper
    nms_thesh = float(0.06)
    start = 0
    if allow_CUDA:
        CUDA = torch.cuda.is_available() # Added for GPU support (disable if insufficient memory is available with flag at top)
    else:
        CUDA = 0
    print("Using CUDA: ", CUDA) # Whether CUDA is enabled (1)/disabled (0)
    num_classes = 2 # two classes: seeds and radicles
    classes = load_classes('data/nioo.names') # names of the classes

    #Set up the neural network
    print("Loading DISCount weights file")
    model = Darknet("config/nioo_15-05-2019_608.cfg") # location of configuration file
    model.load_weights("config/nioo_15-05-2019_608_9000.weights") # Location of weights file
    print("Network successfully loaded")

    model.net_info["height"] = 608 # use 608 pixel resolution for best results (anchor-box dimensions have been tuned to this resolution using K-means clustering)
    print("Using resolution: ", model.net_info["height"])
    inp_dim = int(model.net_info["height"])
    assert inp_dim % 32 == 0
    assert inp_dim > 32

    if CUDA:
        model.cuda()

    model.eval() # set model to evaluation mode
    read_dir = time.time()

    #Detection phase
    try:
        imlist = [osp.join(osp.realpath('.'), images, img) for img in os.listdir(images) if os.path.splitext(img)[1] == '.png' or os.path.splitext(img)[1] =='.jpeg' or os.path.splitext(img)[1] =='.jpg']
    except NotADirectoryError:
        imlist = []
        imlist.append(osp.join(osp.realpath('.'), images))
    except FileNotFoundError:
        print ("No file or directory with the name {}".format(images))
        exit()

    if not os.path.exists("../evaluated"):
        os.makedirs("../evaluated")

    load_batch = time.time()
    batches = list(map(prep_image, imlist, [inp_dim for x in range(len(imlist))]))
    im_batches = [x[0] for x in batches]
    orig_ims = [x[1] for x in batches]
    im_dim_list = [x[2] for x in batches]
    im_dim_list = torch.FloatTensor(im_dim_list).repeat(1,2)

    if CUDA:
        im_dim_list = im_dim_list.cuda()
    leftover = 0

    if (len(im_dim_list) % batch_size):
        leftover = 1

    if batch_size != 1:
        num_batches = len(imlist) // batch_size + leftover
        im_batches = [torch.cat((im_batches[i*batch_size : min((i +  1)*batch_size,
                            len(im_batches))]))  for i in range(num_batches)]
    i = 0
    write =  False
    objs = {}
    start_det_loop = time.time()
    with open('evaluation.csv', 'w', newline='') as csvfile:
        image_count = 0
        for batch in im_batches:
            image_count += 1
            start = time.time()

            if CUDA:
                batch = batch.cuda()

            with torch.no_grad():
                prediction = model(Variable(batch), CUDA)

            prediction = write_results(prediction, confidence, num_classes, nms = True, nms_conf = nms_thesh)

            if type(prediction) == int:
                i += 1
                continue
            end = time.time()

            prediction[:,0] += i*batch_size

            if not write:
                output = prediction
                write = 1
            else:
                output = torch.cat((output,prediction))

            for im_num, image in enumerate(imlist[i*batch_size: min((i +  1)*batch_size, len(imlist))]):
                im_id = i*batch_size + im_num
                objs = [classes[int(x[-1])] for x in output if int(x[0]) == im_id]
                print("Number:", image_count)
                print("{0:20s} predicted in {1:6.2f} seconds".format(image.split("/")[-1], (end - start)/batch_size))
                count = collections.Counter(objs)
                percentage = (count['Radicle']/count['Seed'])#*100
                percentage = round(percentage, 4)
                image_name = image.split("/")[-1]
                germination_writer = csv.writer(csvfile, delimiter=',',
                                        quotechar='|', quoting=csv.QUOTE_MINIMAL)
                germination_writer.writerow([image_name, count['Seed'], count['Radicle'], percentage])
                print("Seeds: {}, Radicles: {}, Germination Percentage: {}".format(count['Seed'], count['Radicle'], "%.2f" % (percentage * 100)))
                print("----------------------------------------------------------")
            i += 1

            if CUDA:
                torch.cuda.synchronize()
    try:
        output
    except NameError:
        print("No detections were made")
        exit()



    im_dim_list = torch.index_select(im_dim_list, 0, output[:,0].long())
    scaling_factor = torch.min(inp_dim/im_dim_list,1)[0].view(-1,1)

    output[:,[1,3]] -= (inp_dim - scaling_factor*im_dim_list[:,0].view(-1,1))/2
    output[:,[2,4]] -= (inp_dim - scaling_factor*im_dim_list[:,1].view(-1,1))/2
    output[:,1:5] /= scaling_factor
    for i in range(output.shape[0]):
        output[i, [1,3]] = torch.clamp(output[i, [1,3]], 0.0, im_dim_list[i,0])
        output[i, [2,4]] = torch.clamp(output[i, [2,4]], 0.0, im_dim_list[i,1])

    output_recast = time.time()
    class_load = time.time()
    draw = time.time()

    def write(x, batches, results):
        c1 = (int(x[1]),int(x[2])) #used to be: c1 = tuple(x[1:3].int())
        c2 = (int(x[3]),int(x[4])) #used to be: c2 = tuple(x[3:5].int())
        img = results[int(x[0])]
        cls = int(x[-1])
        label = "{0}".format(classes[cls])
        if label == 'Seed':
            color = (255, 0, 255)
        else:
            color = (0, 255, 0)
        cv2.rectangle(img, c1,c2, color,3)
        t_size = cv2.getTextSize(label, cv2.FONT_HERSHEY_DUPLEX, 1 , 1)[0]
        c2 = int(x[1]) + t_size[0] + 3, int(x[2])+ t_size[1] + 4
        cv2.rectangle(img, (int(x[1]),int(x[2])),c2, color, -1)
        cv2.putText(img, label, (int(x[1]), int(x[2]) + t_size[1] + 4), cv2.FONT_HERSHEY_DUPLEX, 1, [0,0,0], 1)
        #cv2.rectangle(img, c1, c2,color, 3)
        #t_size = cv2.getTextSize(label, cv2.FONT_HERSHEY_DUPLEX, 1 , 1)[0]
        #c2 = c1[0] + t_size[0] + 3, c1[1] + t_size[1] + 4
        #cv2.rectangle(img, c1, c2,color, -1)
        #cv2.putText(img, label, (c1[0], c1[1] + t_size[1] + 4), cv2.FONT_HERSHEY_DUPLEX, 1, [0,0,0], 1)
        return img

    list(map(lambda x: write(x, im_batches, orig_ims), output))

    det_names = pd.Series(imlist).apply(lambda x: "{}/{}".format("../evaluated",PurePath(x).parts[-1])) #

    list(map(cv2.imwrite, det_names, orig_ims))

    end = time.time()

    print()
    print("Results")
    print("----------------------------------------------------------")
    print("Evaluation of input results stored in 'evaluation.csv' file")
    print("Images with detections stored in the 'evaluated' folder")
    print()
    print("{:25s}: {}".format("Task", "Time Taken (in seconds)"))
    print()
    print("{:25s}: {:2.3f}".format("Detection (" + str(len(imlist)) +  " images)", output_recast - start_det_loop))
    print("{:25s}: {:2.3f}".format("Saving annotations", end - draw))
    print("{:25s}: {:2.3f}".format("Average time_per_img", (end - load_batch)/len(imlist)))
    print("----------------------------------------------------------")

    torch.cuda.empty_cache()
